#!/usr/bin/env python3

# inspired from https://github.com/saiduc/PyOpenGLobe

import sys
import pygame
import math
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from PIL import Image
import numpy


def read_texture(filename):
    """
    Reads an image file and converts to a OpenGL-readable textID format
    """
    _im = Image.open(filename)
    img = _im.transpose(Image.FLIP_LEFT_RIGHT)
    img_data = numpy.array(list(img.getdata()), numpy.int8)
    textID = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, textID)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img.size[0], img.size[1], 0, GL_RGB, GL_UNSIGNED_BYTE, img_data)
    return (textID)

def rotate_globe(dx, dy):
    modelView = (GLfloat * 16)()
    mvm = glGetFloatv(GL_MODELVIEW_MATRIX, modelView)
    # To combine x-axis and y-axis rotation
    temp = (GLfloat * 3)()
    temp[0] = modelView[0]*dy + modelView[1]*dx
    temp[1] = modelView[4]*dy + modelView[5]*dx
    temp[2] = modelView[8]*dy + modelView[9]*dx
    norm_xy = math.sqrt(temp[0]*temp[0] + temp[1]*temp[1] + temp[2]*temp[2])
    if norm_xy:
        glRotatef(math.sqrt(dx*dx+dy*dy),
                  temp[0]/norm_xy,
                  temp[1]/norm_xy,
                  temp[2]/norm_xy)

def static_var(varname, value):
    def decorate(func):
        setattr(func, varname, value)
        return (func)
    return (decorate)


@static_var("compteur", 0)
def take_screenshot(data):
    if 'fd' not in data or not data['fd']:
        data['fd'] = open("balek.rv24", "wb")
    data['fd'].write(glReadPixels(0,0, 400, 400, GL_RGB, GL_UNSIGNED_BYTE))
    #screen = pygame.display.set_mode((400, 500))
    #rect = pygame.Rect(0, 0, data['screen_size'][0], data['screen_size'][0])
    #all = data['screen'].subsurface(rect)
    #pygame.image.save(pygame.display, "./out/image_%04d.bmp" % take_screenshot.compteur)
    print("Screenshot: image_%04d.bmp" % take_screenshot.compteur)
    take_screenshot.compteur += 1

def update(data):
    for event in pygame.event.get():
        if event.type == pygame.QUIT: # QUIT
            pygame.quit()
            quit()
        if event.type == VIDEORESIZE:
            print("RESIZE:", event.dict['size'])
            if event.dict['size'][0] and event.dict['size'][1]: # avoid division by zero
                # oldX / newX , oldY / newY
                size = (data['screen_size'][0] / event.dict['size'][0],
                        data['screen_size'][1] / event.dict['size'][1])
                glScaled(size[0], size[1], size[1]) # XXX
                data['screen_size'] = event.dict['size']
        # Rotation with arrow keys
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE: # QUIT
                exit(0)
            if event.key == pygame.K_p:
                take_screenshot(data)
            if event.key == pygame.K_LEFT:
                rotate_globe(0.1 / data['zoom'], 0)
            if event.key == pygame.K_RIGHT:
                rotate_globe(-0.1 / data['zoom'], 0)
            if event.key == pygame.K_UP:
                rotate_globe(0, 0.1 / data['zoom'])
            if event.key == pygame.K_DOWN:
                rotate_globe(0, -0.1 / data['zoom'])
        # Zoom in and out with mouse wheel
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 4 and data['zoom'] * 1.05 < 4.8:  # wheel rolled up
                data['zoom'] *= 1.05
                glScaled(1.05, 1.05, 1.05)
            if event.button == 5 and data['zoom'] * 0.95 > .1:  # wheel rolled down
                data['zoom'] *= 0.95
                glScaled(0.95, 0.95, 0.95)
            print("ZOOM", "%.02f" % data['zoom'])
         # Rotate with mouse click and drag
        if event.type == pygame.MOUSEMOTION:
            x, y = event.pos
            dx = (x - data['lastPosX']) / (data['zoom'] * data['zoom'])
            dy = (y - data['lastPosY']) / (data['zoom'] * data['zoom'])
            mouseState = pygame.mouse.get_pressed()
            if mouseState[0]:
                rotate_globe(dx, dy)
            data['lastPosX'] = x
            data['lastPosY'] = y
    return (data)



def draw(data):
    glEnable(GL_DEPTH_TEST)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    # Creates Sphere and wraps texture
    qobj = gluNewQuadric()
    gluQuadricTexture(qobj, GL_TRUE)
    glEnable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, data['texture'])
    gluSphere(qobj, 1, 50, 50)
    gluDeleteQuadric(qobj)
    glDisable(GL_TEXTURE_2D)
    # Displays pygame window
    pygame.display.flip()
    pygame.time.wait(10)

def loop(data):
    while True:
        update(data)
        draw(data)

def main(filename):
    pygame.init()
    display = (400, 400)
    screen = pygame.display.set_mode(display, DOUBLEBUF | OPENGL | RESIZABLE)
    pygame.display.set_caption('Globe')
    pygame.key.set_repeat(1, 10)    # allows press and hold of buttons
    #gluPerspective(40, (display[0]/display[1]), 0.1, 50.0)
    gluPerspective(40, 1, 0.1, 50.0)
    glTranslatef(0.0, 0.0, -5)    # sets initial zoom so we can see globe
    rotate_globe(0, 90) # vertical polar axe
    image = read_texture(filename)
    print("Image file loaded !")
    data = {
        'zoom': 1.0,
        'lastPosX': 0,
        'lastPosY': 0,
        'texture': image,
        'screen_size': display,
        'screen': screen,
    }
    loop(data)





##########################################
##  ENTRYPOINT                          ##
##########################################

if __name__ == '__main__':
    if len(sys.argv) != 2:
        usage = """
        Globe image displayer
        Usage: %s <filename>
        """ % sys.argv[0]
        print(usage)
    else:
        main(sys.argv[1])
